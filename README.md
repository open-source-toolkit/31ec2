# Camunda Web 页面汉化文件

## 简介

本仓库提供了一个用于 Camunda Web 页面的汉化文件，适用于 Camunda 7.15.19 版本。该汉化文件兼容 Camunda 7.0 及以上版本，方便用户在使用 Camunda Web 页面时获得更好的中文体验。

## 使用方法

1. **下载汉化文件**：
   - 点击仓库中的 `camunda-web-chinese.json` 文件进行下载。

2. **替换或导入汉化文件**：
   - 将下载的 `camunda-web-chinese.json` 文件放置到 Camunda Web 页面的相应目录中，或按照 Camunda 的国际化配置方式导入该文件。

3. **重启 Camunda Web 页面**：
   - 完成文件替换或导入后，重启 Camunda Web 页面，即可看到汉化后的界面。

## 注意事项

- 本汉化文件仅适用于 Camunda 7.0 及以上版本。
- 如果在使用过程中遇到任何问题，欢迎在仓库中提交 Issue 或 Pull Request。

## 贡献

欢迎大家贡献代码或提出改进建议，帮助我们完善 Camunda Web 页面的汉化工作。

## 许可证

本项目采用 MIT 许可证，详情请参阅 [LICENSE](LICENSE) 文件。